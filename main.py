import math
from scales import Scales
from player import Player

scales = Scales()
player = Player()

note = input("Choose a note (e.g. C or F#)")
if note == "":
    note = "C"
octave = input("Choose an octave (e.g. 3 or 4)")
if octave == "":
    octave = "4"
scale_type = input("Choose a scale type (major, minor, or blues)")
if scale_type == "":
    scale_type = "major"

scale = scales.get_scale(note + octave, scale_type, include_self=True)
double_scale = scale[:-1].copy()
scale.reverse()
double_scale = double_scale + scale
halfway = math.floor(len(double_scale) / 2)
for index, note in enumerate(double_scale):
    note_duration = (abs(halfway - index) * 20) + 50
    player.play_note(note, note_duration)
