import time
import math
import random
import threading

from scales import Scales
from player import Player

scales = Scales()


class BluesPlayer:
    def __init__(self, instrument_type, notes):
        self.instrument = instrument_type
        self.notes = notes
        self.player = Player(instrument_type)

    def play_blues(self):
        print(f"Starting to play {self.instrument}")
        num_notes = len(self.notes)
        note_index = math.floor(num_notes / 2)

        while True:
            if random.random() > 0.5:
                note_index += 1
            else:
                note_index -= 1

            if note_index < 0:
                note_index = 0
            elif note_index >= num_notes:
                note_index = num_notes - 1

            duration = random.randint(2, 8) * 30

            print(
                f"Playing a note {self.instrument}, {self.notes[note_index]} for {duration}ms"
            )
            self.player.play_note(self.notes[note_index], duration)
            # time.sleep(duration / 1000)


def BrassDude():
    brass_notes = (
        scales.get_scale("C3", "blues")
        + scales.get_scale("C4", "blues")
        + scales.get_scale("C5", "blues")
    )
    brass_dude = BluesPlayer("brass", brass_notes)
    brass_dude.play_blues()


def SynthLady():
    synth_notes = (
        scales.get_scale("C3", "blues")
        + scales.get_scale("C4", "blues")
        + scales.get_scale("C5", "blues")
    )
    synth_lady = BluesPlayer("sine", synth_notes)
    synth_lady.play_blues()


SynthLady()
