from pydub import AudioSegment
from pydub.generators import Sine
from pydub.playback import play


class Player:
    TEMPO = 500

    def __init__(self, instrument_type="sine"):
        self.instrument = instrument_type

    def note_to_frequency(self, note):
        c4_frequencies = {
            "C": 261.63,
            "C#": 277.18,
            "D": 293.66,
            "D#": 311.13,
            "E": 329.63,
            "F": 349.23,
            "F#": 369.99,
            "G": 392.00,
            "G#": 415.30,
            "A": 440.00,
            "A#": 466.16,
            "B": 493.88,
        }

        note_name = note[:-1]
        octave = int(note[-1])

        c4_frequency = c4_frequencies.get(note_name)

        if c4_frequency is None:
            return "Invalid note"

        return c4_frequency * (2 ** (octave - 4))

    def get_duration_ratio(self, note):
        match note[0:1]:
            case "4":
                return 1
            case "2":
                return 0.5
            case "1":
                return 0.25
            case _:
                return 1

    def play_notes(self, note_list, duration=500):
        for note in note_list:
            # check if the note is in ["C4", "𝅘𝅥𝅯"] format, or just "C4"
            if isinstance(note, list):
                duration = self.get_duration_ratio(note[1]) * self.TEMPO
                self.play_note(note[0], duration)
            else:
                self.play_note(note, duration)

    def play_note(self, note, duration=500):
        frequency = self.note_to_frequency(note)

        wave = Sine(frequency).to_audio_segment(duration=duration)
        if self.instrument == "plucked":
            harmonics1 = (
                Sine(frequency * 2).to_audio_segment(duration=duration).apply_gain(-3)
            )  # Halve the amplitude
            harmonics2 = (
                Sine(frequency * 4).to_audio_segment(duration=duration).apply_gain(-6)
            )  # Quarter the amplitude
            wave = wave.overlay(harmonics1).overlay(harmonics2)
        elif self.instrument == "brass":
            # Add fifth and octave harmonics to simulate a brass-like sound
            harmonics1 = (
                Sine(frequency * 1.5).to_audio_segment(duration=duration).apply_gain(-6)
            )
            harmonics2 = (
                Sine(frequency * 2).to_audio_segment(duration=duration).apply_gain(-12)
            )
            wave = wave.overlay(harmonics1).overlay(harmonics2)
        elif self.instrument == "reed":
            # Add third and fifth harmonics to simulate a reed-like sound
            harmonics1 = (
                Sine(frequency * 1.33)
                .to_audio_segment(duration=duration)
                .apply_gain(-6)
            )
            harmonics2 = (
                Sine(frequency * 1.5)
                .to_audio_segment(duration=duration)
                .apply_gain(-12)
            )
            wave = wave.overlay(harmonics1).overlay(harmonics2)

        if self.instrument in ["plucked", "brass", "reed"]:
            sample = wave.fade_in(50).fade_out(200)
            sample = sample.low_pass_filter(1500)
        else:
            sample = wave
            # .fade_in(15).fade_out(15)

        play(sample)
