class Scales:
    def get_scale_type(self, name):
        match name:
            case "major":
                # Major scale: W-W-H-W-W-W-H
                return [0, 2, 4, 5, 7, 9, 11]
            case "minor":
                # Natural minor scale: W-H-W-W-H-W-W
                return [0, 2, 3, 5, 7, 8, 10]
            case "blues":
                # Blues scale: m3-W-W-H-H-W
                return [0, 3, 5, 6, 7, 10]

    def get_scale(self, root, scale_type_name="major", include_self=False):
        notes = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]
        root_index = notes.index(root[:-1])
        octave = int(root[-1])

        if root_index == -1:
            return ["Invalid root note"]

        scale_notes = []

        pattern = self.get_scale_type(scale_type_name)

        if include_self:
            pattern.append(12)

        for step in pattern:
            note_index = (root_index + step) % 12
            note_octave = octave + ((root_index + step) // 12)
            note_name = f"{notes[note_index]}{note_octave}"

            scale_notes.append(note_name)

        return scale_notes
