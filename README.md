# Some trivial experimentation using pydub/pyaudio

## Instructions

*   `brew install portaudio && brew link portaudio`
*   `pip install -r requirements.txt`

## Examples

### main.py
*   `python3 main.py`
*   Will ask you some questions, then play you a scale

### doodle.py
*   `python3 doodle.py`
*   Will play you a tune

### blues.py
*   `python3 blues.py`
*   Will play the blues (forever)
